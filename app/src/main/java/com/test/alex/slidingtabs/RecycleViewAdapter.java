package com.test.alex.slidingtabs;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Alex on 9/15/2015.
 */
public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder>{
    private ArrayList<Event> mDataset;
    private static Context context;
  //  private static ArrayList<View> cardViews=new ArrayList<View>();



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder  {
        // each data item is just a string in this case
        public View mView;



        public ViewHolder(View v) {
            super(v);
            mView = v;
      //  cardViews.add(mView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecycleViewAdapter(Context activityContext,ArrayList<Event> myDataset) {

        mDataset = myDataset;
        context=activityContext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
     //   TextView eventName=(TextView)holder.mView.findViewById(R.id.event_name);
        //TextView eventLocation=(TextView)holder.mView.findViewById(R.id.event_location);
        TextView eventHour=(TextView)holder.mView.findViewById(R.id.event_time);

     //   eventName.setText(mDataset.get(position).getName());
       // eventLocation.setText(mDataset.get(position).getLocation());
        eventHour.setText(mDataset.get(position).gethour());
        final int pos=position;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Toast.makeText(context,new Integer(Build.VERSION.SDK_INT)+"Bigger than"+new Integer(Build.VERSION_CODES.LOLLIPOP),Toast.LENGTH_SHORT).show();

            holder.mView.setOnTouchListener(new View.OnTouchListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v
                            .findViewById(R.id.cv)
                            .getBackground()
                            .setHotspot(event.getX(), event.getY());
                  //  Toast.makeText(context,new Float(event.getX())+" "+new Float(event.getY()),Toast.LENGTH_SHORT).show();

                    return (false);
                }
            });
            if(position==0)
                holder.mView.setPadding(0,24,0,0);

        }
        else{
            Toast.makeText(context,new Integer(Build.VERSION.SDK_INT)+"Bigger than"+new Integer(Build.VERSION_CODES.LOLLIPOP),Toast.LENGTH_SHORT).show();
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context,"Pressed inner "+ new Integer(pos),Toast.LENGTH_SHORT).show();

            }
        });

        RecyclerView avatarListRV=(RecyclerView)holder.mView.findViewById(R.id.event_recycler_view);
        RecyclerView.Adapter mAdapter;
        LinearLayoutManager mLayoutManager;
        avatarListRV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        avatarListRV.setLayoutManager(mLayoutManager);
        ArrayList <Integer> dataSet=new ArrayList<>();
        for(int i=0;i<10;i++)
            dataSet.add(new Integer(R.drawable.profile));
        dataSet.add(new Integer(R.drawable.profile));
        dataSet.add(new Integer(R.drawable.profile));
        dataSet.add(new Integer(R.drawable.profile));
        mAdapter=new RecycleViewAdapterAvatar(context,dataSet);
        avatarListRV.setAdapter(mAdapter);

        ImageView eventPhoto=(ImageView)holder.mView.findViewById(R.id.event_photo);
        Drawable photo = context.getResources().getDrawable(mDataset.get(position).getPhoto() );
        eventPhoto.setBackground(photo);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
