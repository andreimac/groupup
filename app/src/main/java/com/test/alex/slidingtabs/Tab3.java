package com.test.alex.slidingtabs;

/**
 * Created by Alex on 9/19/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class Tab3 extends Fragment {

    private View displayView;
    private Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.contacts_layout,container,false);
        displayView=v;
        context=getActivity().getApplicationContext();

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ListView contactList=(ListView)displayView.findViewById(R.id.contact_list);
        ArrayList<User> contactSet=new ArrayList<>();
        for(int i=0;i<10;i++)
            contactSet.add(new User(R.drawable.profile,"Visarion Alex","Be freeeee!!!!"));
        contactList.setAdapter(new ListViewAdapterContacts(context,R.id.contact_row,contactSet));
    }

}