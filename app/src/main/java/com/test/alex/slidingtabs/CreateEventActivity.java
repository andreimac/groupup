package com.test.alex.slidingtabs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rey.material.widget.CheckBox;
import com.rey.material.widget.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class CreateEventActivity extends AppCompatActivity {

    private DatePickerDialog oneDatePickerDialogStart;
    private DatePickerDialog oneDatePickerDialogEnd;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog oneTimePickerDialogStart;
    private TimePickerDialog oneTimePickerDialogEnd;



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Location selection cancelled", Toast.LENGTH_SHORT).show();
        }
        else if(requestCode==1) {
            Bundle received=data.getExtras();
            //  Toast.makeText(this, received.getString("Coordonate"),Toast.LENGTH_SHORT).show();
            EditText editLocation=(EditText)this.findViewById(R.id.edit_location);
            editLocation.setText(received.getString("Coordonate"));

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        getSupportActionBar().hide();
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.ColorPrimaryDark));

        setContentView(R.layout.activity_create_event);

        this.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);


        // final Button btnCreate=(Button)this.findViewById(R.id.button_create_event);
        // final Button btnCancel=(Button)this.findViewById(R.id.button_cancel_event);

        final EditText editName=(EditText)this.findViewById(R.id.edit_name);
        final EditText editDetails=(EditText)this.findViewById(R.id.edit_description_text);
        final EditText editLocation=(EditText)this.findViewById(R.id.edit_location);
        final EditText editStartDate=(EditText)this.findViewById(R.id.edit_start_date);
        final EditText editEndDate=(EditText)this.findViewById(R.id.edit_end_date);

        final EditText editStartHour=(EditText)this.findViewById(R.id.edit_start_hour);
        final EditText editEndHour=(EditText)this.findViewById(R.id.edit_end_hour);

        final CardView locationCard =(CardView)this.findViewById(R.id.cardview_location);
        // final Spinner spinnerPrivacy=(Spinner)this.findViewById(R.id.spinnerFormPrivacy);
        // final Spinner spinnerNumber=(Spinner)this.findViewById(R.id.spinnerFormNumber);
        // final Spinner spinnerActivity=(Spinner)this.findViewById(R.id.spinnerFormActivity);
        final Context context=this;

        final Event.Builder builder=new Event.Builder();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        //Set up the time widhets, using the calendarr
        oneDatePickerDialogStart = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editStartDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        oneDatePickerDialogEnd = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editEndDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        oneTimePickerDialogStart=new TimePickerDialog(this,new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                editStartHour.setText(hourOfDay + ":" + minute);

            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);

        oneTimePickerDialogEnd=new TimePickerDialog(this,new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                editEndHour.setText(hourOfDay + ":" + minute);

            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);

        locationCard.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent displayMap=new Intent(context,MapsActivity.class);
                ((Activity) context).startActivityForResult(displayMap, 1);

            }
        });
        editLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationCard.performClick();
            }
        });

        editStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                oneDatePickerDialogStart.show();


            }
        });
        editEndDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                oneDatePickerDialogEnd.show();
            }
        });

        editStartHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                oneTimePickerDialogStart.show();

            }
        });
        editEndHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                oneTimePickerDialogEnd.show();

            }
        });


        ListView listViewInvite=(ListView)this.findViewById(R.id.invite_list);
        ArrayList<User> contactSet=new ArrayList<>();
        contactSet.add(new User(R.drawable.profile,"Visarion Alex","Be freeeee!!!!"));
        contactSet.add(new User(R.drawable.profile,"Grigorean Alin","Every choice is a piece of your future self."));
        contactSet.add(new User(R.drawable.profile,"Caciula Raluca","Live,Laugh,Love."));

        for(int i=0;i<8;i++)
            contactSet.add(new User(R.drawable.profile,"Visarion Alex","Be freeeee!!!!"));
        ListViewAdapterInviteList adapterInviteList=new ListViewAdapterInviteList(context,R.id.invited_row,contactSet);
        listViewInvite.setAdapter(adapterInviteList);
         Utils.setListViewHeightBasedOnChildren(listViewInvite);





        final com.rey.material.widget.Spinner spn_label = (com.rey.material.widget.Spinner)this.findViewById(R.id.spinner_activities);
        String[] items = new String[20];
        items[0]="Piesa de teatru";
        items[1]="Plimbare";
        items[2]="Film";
        items[3]="Bowling";
        items[4]="Ratele si vanatorii";
        items[5]="Patinoar";
        for(int i = 6; i < items.length; i++)
            items[i] = "Item " + String.valueOf(i + 1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.row_spn, items);
        adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
        spn_label.setAdapter(adapter);


        final CheckBox customActivityCheck=(CheckBox)this.findViewById(R.id.custom_activity_checkBox);
        final EditText editNewActivity=(EditText)this.findViewById(R.id.edit_new_activity);
       // final ImageView triangle=(ImageView)this.findViewById(R.id.triangle);
        final TranslateAnimation translateDown= new TranslateAnimation(0,0,0,150);
        translateDown.setDuration(300);
        translateDown.setFillAfter(true);
        translateDown.setInterpolator(new AccelerateInterpolator());

        final TranslateAnimation translateUp= new TranslateAnimation(0,0,150,0);
        translateUp.setDuration(300);
        translateUp.setFillAfter(true);
        translateUp.setInterpolator(new DecelerateInterpolator());

        /*
        customActivityCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    triangle.startAnimation(translateDown);
                }
                else triangle.startAnimation(translateUp);
            }
        });
//        */

        /*
        final Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setDuration(500);
        fadeOut.setFillAfter(true);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (customActivityCheck.isChecked())
                    spn_label.setVisibility(View.GONE);
                else
                    editNewActivity.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(500);
        fadeIn.setFillAfter(true);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (customActivityCheck.isChecked())
                    editNewActivity.setVisibility(View.VISIBLE);
                else
                    spn_label.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Utils.disableEditText(editNewActivity);
        customActivityCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    spn_label.startAnimation(fadeOut);
                    spn_label.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            editNewActivity.startAnimation(fadeIn);
                            editNewActivity.setFocusableInTouchMode(true);
                        }
                    }, 700);
                    //  editNewActivity.startAnimation(fadeIn);
                    // spn_label.setVisibility(View.GONE);
                    //  editNewActivity.setVisibility(View.VISIBLE);
                } else {
                    editNewActivity.startAnimation(fadeOut);

                    editNewActivity.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.disableEditText(editNewActivity);
                            spn_label.startAnimation(fadeIn);
                        }
                    }, 700);
                    // spn_label.setVisibility(View.VISIBLE);
                    //editNewActivity.setVisibility(View.GONE);
                }
            }
        });

        /*

        btnCreate.setOnClickListener(new View.OnClickListener() {

            //De incarcat pe net
            Event newEvent;
            @Override
            public void onClick(View v) {
                String name=editName.getText().toString();
                String details=editDetails.getText().toString();
                String location=editLocation.getText().toString();
                String date=editDate.getText().toString();
                String hour=editHour.getText().toString();
                String privacy=spinnerPrivacy.getSelectedItem().toString();
                String number=spinnerNumber.getSelectedItem().toString();
                String activity=spinnerActivity.getSelectedItem().toString();
                if(name.equals("")||details.equals("")||privacy.equals("")||number.equals("")||date.equals("")||hour.equals(""))
                    Toast.makeText(context, "Please enter all the information. ", Toast.LENGTH_SHORT).show();
                else{
                    newEvent=builder
                            .name(name)
                            .details(details)
                            .date(date)
                            .hour(hour)
                            .location(location)
                            .privacy(privacy)
                            .numberOfPeople(number)
                            .activity(activity)
                            .build();
                    Toast.makeText(context, "Event created", Toast.LENGTH_SHORT).show();
                    //Write event !
                    File file = new File(context.getFilesDir(),"events.mu");
                    try {
                        FileOutputStream fout = new FileOutputStream(file);
                        ObjectOutputStream oos = new ObjectOutputStream(fout);
                        oos.writeObject(newEvent);
                        oos.close();
                    } catch (FileNotFoundException e) {
                        String string=new String("Nu s-a putut scrie in memoria interna");
                        Toast toast=Toast.makeText(context, string, Toast.LENGTH_SHORT);
                        toast.show();
                        e.printStackTrace();
                    } catch (IOException e) {
                        String string=new String("Nu s-a putut crea evenimentul");
                        Toast toast=Toast.makeText(context, string, Toast.LENGTH_SHORT);
                        toast.show();
                        e.printStackTrace();
                    }

                    //Upload event
                    Socket client;
                    try {
                        client = new Socket("141.85.159.121", 4441);
                        ObjectOutputStream outToServer = new ObjectOutputStream(client.getOutputStream());
                        outToServer.writeObject("333");
                        client.close();   //closing the connection
                        Intent nou=new Intent(context,MainActivity.class);
                        context.startActivity(nou);
                    } catch (IOException e) {
                        afisare("Couldn't connect to the server.");
                        e.printStackTrace();
                        Intent nou=new Intent(context,MainActivity.class);
                        context.startActivity(nou);
                    }

                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nou=new Intent(context,MainActivity.class);
                context.startActivity(nou);
            }
        });
        */

    }
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //  this.finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.create_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //   if (id == R.id.action_settings) {return true;}
        return super.onOptionsItemSelected(item);
    }


}
