package com.test.alex.slidingtabs;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Alex on 9/15/2015.
 */
public class RecycleViewAdapterAvatar extends RecyclerView.Adapter<RecycleViewAdapterAvatar.ViewHolder>{
    private ArrayList<Integer> mDataset;
    private int size;
    private static Context context;
    //  private static ArrayList<View> cardViews=new ArrayList<View>();



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder  {
        // each data item is just a string in this case
        public View mView;



        public ViewHolder(View v) {
            super(v);
            mView = v;
            //  cardViews.add(mView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecycleViewAdapterAvatar(Context activityContext,ArrayList<Integer> myDataset) {

        mDataset = myDataset;
        context=activityContext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecycleViewAdapterAvatar.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.avatar, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //   TextView eventName=(TextView)holder.mView.findViewById(R.id.event_name);

      ImageView avatar=(ImageView)holder.mView.findViewById(R.id.avatar_img);
        avatar.setImageResource(mDataset.get(position).intValue());

        final int pos=position;

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Pressed inner " + new Integer(pos), Toast.LENGTH_SHORT).show();

            }
        });



    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        Toast.makeText(context,new Integer(mDataset.size()).toString(),Toast.LENGTH_SHORT);
        return mDataset.size();
    }
}
