package com.test.alex.slidingtabs;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;

public class ListViewAdapterInviteList extends ArrayAdapter<User> {
    private  Context context;
    private  ArrayList<User> contactList=null;

    public ListViewAdapterInviteList(Context context,int layout_row, ArrayList<User> contactList) {
        super(context, layout_row,contactList);
        this.context = context;
        this.contactList=contactList;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.invited_row, parent, false);
        TextView textViewNume = (TextView) rowView.findViewById(R.id.numeContact);
        final com.rey.material.widget.CheckBox checkBoxStatus = (com.rey.material.widget.CheckBox) rowView.findViewById(R.id.invitation_status);
        ImageView imageViewContact = (ImageView) rowView.findViewById(R.id.avatar_contact);


        textViewNume.setText(contactList.get(position).getName());
        checkBoxStatus.setChecked(contactList.get(position).isInvited());
        imageViewContact.setImageResource(contactList.get(position).getPhoto());
        checkBoxStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxStatus.setChecked(!contactList.get(position).isInvited());
                contactList.get(position).setInvited(!contactList.get(position).isInvited());
            }
        });




        return rowView;
    }
}