package com.test.alex.slidingtabs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;

public class ListViewAdapterContacts extends ArrayAdapter<User> {
    private  Context context;
    private  ArrayList<User> contactList=null;

    public ListViewAdapterContacts(Context context,int layout_row, ArrayList<User> contactList) {
        super(context, layout_row,contactList);
        this.context = context;
        this.contactList=contactList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.contact_row, parent, false);
        TextView textViewNume = (TextView) rowView.findViewById(R.id.numeContact);
        TextView textViewStatus = (TextView) rowView.findViewById(R.id.statusContact);
        ImageView imageViewContact = (ImageView) rowView.findViewById(R.id.avatar_contact);


        textViewNume.setText(contactList.get(position).getName());
        textViewStatus.setText(contactList.get(position).getStatus());
        imageViewContact.setImageResource(contactList.get(position).getPhoto());

        if(position==0)
            rowView.setPadding(0,24,0,0);


        return rowView;
    }
}