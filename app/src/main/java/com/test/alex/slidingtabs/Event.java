package com.test.alex.slidingtabs;

import java.io.Serializable;


public class Event implements Serializable{
    private final String privacy;
    private final String numerOfPeople;
    private final String location;
    private final String activity;
    private final String date;
    private final String hour;
    private final String name;
    private final String details;
    private final int photo;
    public static class Builder{
        private  String privacy;
        private  String numerOfPeople;
        private  String location;
        private  String activity;
        private  String date;
        private  String hour;
        private  String name;
        private  String details;
        private int photo;
        public Builder privacy(String privacy){
            this.privacy=privacy;
            return this;
        }
        public Builder numberOfPeople(String nr){
            this.numerOfPeople=nr;
            return this;
        }
        public Builder location(String location){
            this.location=location;
            return this;
        }
        public Builder activity(String activity){
            this.activity=activity;
            return this;
        }
        public Builder date(String data){
            this.date=data;
            return this;
        }
        public Builder hour(String hour){
            this.hour=hour;
            return this;
        }
        public Builder details(String d){
            this.details=d;
            return this;
        }
        public Builder name(String name){
            this.name=name;
            return this;
        }
        public Builder photo(int photo){
            this.photo=photo;
            return this;
        }
        public Event build(){
            return new Event(this);
        }
    }
    private Event(Builder builder){
        this.activity=builder.activity;
        this.location=builder.location;
        this.date=builder.date;
        this.hour=builder.hour;
        this.privacy=builder.privacy;
        this.numerOfPeople=builder.numerOfPeople;
        this.details=builder.details;
        this.name=builder.name;
        this.photo=builder.photo;
    }
    public String getPrivacy() {
        return privacy;
    }
    public String getNumerOfPeople() {
        return numerOfPeople;
    }
    public String getLocation() {
        return location;
    }
    public String getActivity() {
        return activity;
    }
    public String getdate() {
        return date;
    }
    public String gethour() {
        return hour;
    }
    public String getName() {
        return name;
    }
    public String getDetails() {
        return details;
    }
    public int getPhoto(){
        return  photo;
    }
    @Override
    public String toString() {
        return "Event [privacy=" + privacy + ", numerOfPeople=" + numerOfPeople
                + ", location=" + location + ", activity=" + activity
                + ", date=" + date + ", hour=" + hour + ", name=" + name
                + ", details=" + details + "]";
    }

}
