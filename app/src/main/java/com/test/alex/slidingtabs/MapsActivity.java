package com.test.alex.slidingtabs;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Marker;
import com.test.alex.slidingtabs.R;


public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    Marker toAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context=this;
        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        //   View mapScreen=inflater.inflate(R.layout.activity_maps,null);
        //   Button selectLocation=new Button(this);
        setContentView(R.layout.activity_maps);
        Button selectLocation=(Button)this.findViewById(R.id.button_select_location);
        //  selectLocation.setBottom(0);
        //  LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) selectLocation.getLayoutParams();
        //this.addContentView(selectLocation,layoutParams);
        if(selectLocation!=null)
            selectLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mMap==null||toAdd==null)
                        Toast.makeText(context, "First select location!", Toast.LENGTH_SHORT).show();
                    else{
                        Intent returnIntent=new Intent();
                        //   Bundle toReturn=new Bundle();
                        //   toReturn.putString("Coordonate",toAdd.getPosition().toString());
                        returnIntent.putExtra("Coordonate",toAdd.getPosition().toString());
                        setResult(1,returnIntent);
                        finish();
                    }

                }
            });
        else                     Toast.makeText(context, "Null", Toast.LENGTH_SHORT).show();
        setUpMapIfNeeded();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                if (toAdd != null)
                    toAdd.remove();
                Toast.makeText(getApplicationContext(), point.toString(), Toast.LENGTH_SHORT).show();
                toAdd = mMap.addMarker(new MarkerOptions()
                        .position(point)
                        .title("Location")
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_location)));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            // MapView mapView =(MapView)this.findViewById(R.id.mapView01);
            //  mMap= mapView.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        // mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        // final LatLng HAMBURG = new LatLng(53.558, 9.927);
        final LatLng KIEL = new LatLng(53.551, 9.993);
        final LatLng Regie=new LatLng(44.445266,26.050869);
        Marker markerRegie = mMap.addMarker(new MarkerOptions()
                .position(Regie)
                .title("Regie")
                .snippet("Regie is cool")
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_play_dark)));

        // mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Regie, 10));
        mMap.getUiSettings().setZoomControlsEnabled(Boolean.TRUE);



    }
}
