package com.test.alex.slidingtabs;

/**
 * Created by Alex on 9/19/2015.
 */

        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.ImageButton;


public class Tab2 extends Fragment {
    private View displayedView;
    private Context context;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        displayedView = inflater.inflate(R.layout.plus_button,container,false);
        context=this.getActivity().getApplication().getBaseContext();
        return displayedView;
    }
}
