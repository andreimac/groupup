package com.test.alex.slidingtabs;

/**
 * Created by Alex on 9/19/2015.
 */

        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.provider.SyncStateContract;
        import android.support.annotation.Nullable;
        import android.support.design.widget.FloatingActionButton;
        import android.support.v4.app.Fragment;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.GestureDetector;
        import android.view.LayoutInflater;
        import android.view.MotionEvent;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.animation.AccelerateInterpolator;
        import android.view.animation.AlphaAnimation;
        import android.view.animation.Animation;
        import android.view.animation.DecelerateInterpolator;

        import java.util.ArrayList;

/**
 * Created by hp1 on 21-01-2015.
 */
public class Tab1 extends Fragment {

    private Context context;
    private View displayView;
    private FloatingActionButton fab;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context=getActivity().getApplicationContext();
        displayView =inflater.inflate(R.layout.recycle_view,container,false);
        final GestureDetector gesture = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                        Utils.afisare(context,"scroll");
                        return super.onScroll(e1, e2, distanceX, distanceY);

                    }

                    @Override
                    public boolean onDown(MotionEvent e) {
                        Utils.afisare(context,"Down");
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {
                        Utils.afisare(context, "onFling has been called!");
                        final int SWIPE_MIN_DISTANCE = 120;
                        final int SWIPE_MAX_OFF_PATH = 250;
                        final int SWIPE_THRESHOLD_VELOCITY = 200;
                        try {
                            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                                return false;
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Utils.afisare(context,"Right to Left");
                            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Utils.afisare(context, "Left to Right");
                            }
                        } catch (Exception e) {
                            Utils.afisare(context,e.toString());
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

        displayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.afisare(context,"OnTouch");
                return gesture.onTouchEvent(event);
            }
        });


        return displayView;
    }

    @Override
    public void onPause() {
    //    Utils.afisare(this.getContext(), "Yaay out");

        super.onPause();
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);
        fab.hide();
    }

    @Override
    public void onResume() {
     //   Utils.afisare(this.getContext(), "Yaay in");
        super.onResume();
        fab.hide();
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);
        fab.show();

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView mRecyclerView;
        RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;
        //Initializam Recycler Viewer
        mRecyclerView = (RecyclerView) displayView.findViewById(R.id.event_recycler_view);
        fab = (FloatingActionButton)displayView.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newEvent = new Intent(context, CreateEventActivity.class);
                startActivity(newEvent);
            }
        });
        if(mRecyclerView==null)
            return;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this.getActivity().getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        ArrayList<Event> dataset=new ArrayList<Event>();
        Event.Builder builder=new Event.Builder();
        for(int i=0;i<4;i++) {
            dataset.add(builder
                    .name("Herastrau Park Walk")
                    .details("Cea mai tare ieseala")
                    .date("11.02.2094")
                    .hour("13:00")
                    .location("Herastrau")
                    .privacy("Public")
                    .numberOfPeople("12")
                    .activity("Paintball")
                    .photo(R.drawable.theatre)
                    .build());
            dataset.add(builder
                    .name("Star Wars The Force Awakens Movie")
                    .details("Cea mai tare gasca de fotbal")
                    .date("12.04.2016")
                    .hour("11:05")
                    .location("Cinema City")
                    .privacy("Private")
                    .numberOfPeople("11")
                    .activity("Football")
                    .photo(R.drawable.parc)
                    .build());
        }
        mAdapter = new RecycleViewAdapter(context,dataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}

