package com.test.alex.slidingtabs;

/**
 * Created by Alex on 10/7/2015.
 */
public class User {
    private int photo;
    private boolean invited;
    private String name,status;
    public User(int photo,String name,String status){
        this.photo=photo;
        this.name=name;
        this.status=status;
    }

    public int getPhoto() {
        return photo;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }
    public boolean isInvited(){ return invited; }
    public void setInvited(boolean value){ this.invited=value; }
}
